﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Time_Has_Come
{    
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            Title = "Главная";
            Image0_0.Source = ImageSource.FromResource("Time_Has_Come.Photos.Main.fiore_delli_liberi.jpg");            
            Image0_1.Source = ImageSource.FromResource("Time_Has_Come.Photos.Main.strength.jpg");            
            Image1_0.Source = ImageSource.FromResource("Time_Has_Come.Photos.Main.grassi.jpg");            
            Image1_1.Source = ImageSource.FromResource("Time_Has_Come.Photos.Main.halberd.jpg");
            Image2_0.Source = ImageSource.FromResource("Time_Has_Come.Photos.Main.king.jpg");
            Image2_1.Source = ImageSource.FromResource("Time_Has_Come.Photos.Main.plate_armor.jpg");
            Image3_0.Source = ImageSource.FromResource("Time_Has_Come.Photos.Main.duel.jpg");
            Image3_1.Source = ImageSource.FromResource("Time_Has_Come.Photos.Main.duel_axe.jpg");
            Image4_0.Source = ImageSource.FromResource("Time_Has_Come.Photos.Main.history_duel.jpg");
            Image4_1.Source = ImageSource.FromResource("Time_Has_Come.Photos.Main.knifes.jpg");
            black_back0_0.Source = ImageSource.FromResource("Time_Has_Come.Photos.dark.png");
            black_back0_1.Source = ImageSource.FromResource("Time_Has_Come.Photos.dark.png");            
            black_back1_0.Source = ImageSource.FromResource("Time_Has_Come.Photos.dark.png");
            black_back1_0.Clicked += (s, e) => { Page1 page1 = new Page1(); Navigation.PushAsync(page1); };
            black_back1_1.Source = ImageSource.FromResource("Time_Has_Come.Photos.dark.png");
            black_back2_0.Source = ImageSource.FromResource("Time_Has_Come.Photos.dark.png");
            black_back2_1.Source = ImageSource.FromResource("Time_Has_Come.Photos.dark.png");
            black_back3_0.Source = ImageSource.FromResource("Time_Has_Come.Photos.dark.png");
            black_back3_1.Source = ImageSource.FromResource("Time_Has_Come.Photos.dark.png");
            black_back4_0.Source = ImageSource.FromResource("Time_Has_Come.Photos.dark.png");
            black_back4_1.Source = ImageSource.FromResource("Time_Has_Come.Photos.dark.png");
            ToolbarItem toolbar = new ToolbarItem
            {
                Order = ToolbarItemOrder.Default,
                Priority = 0,
                IconImageSource = new FileImageSource
                {
                    File = "open_menu.pgn"
                }
            };
            ToolbarItem toolbar2 = new ToolbarItem
            {
                Order = ToolbarItemOrder.Primary,
                Priority = 1,
                IconImageSource = new FileImageSource
                {
                    File = "search.pgn"
                }
            };
            ToolbarItems.Add(toolbar);
            ToolbarItems.Add(toolbar2);
        }
    }
}
//private async void GayButton_Clicked(object sender, EventArgs e)
//{
//    string result = await DisplayActionSheet("Кто же ты?", "Натурал", null, "Гачист", "Маскулист", "Онанист");
//    switch (result)
//    {
//        case "Гачист":
//            Editor1.Text = "Извините, милорд! Не заметил!";
//            break;
//        case "Маскулист":
//            Editor1.Text = "МММУУУУУжчина! Уважаем!";
//            break;
//        case "Онанист":
//            Editor1.Text = "Ну а кто не без греха!";
//            break;
//        default:
//            Editor1.Text = "Уходи...";
//            await DisplayAlert("Фууууу", "Мы таких не уважаем! Ты предал Гачимучеников!", "Но я...", "Извините...");
//            break;
//    }
//}
//private async void Where_Clicked(object sender, EventArgs e)
//{
//    await Navigation.PushAsync(new Page1());
//}
//Button1.Clicked += (s, e) => Button1.Text = "Ты пидор";
//            Button1.Clicked += (s, e) =>
//            {
//                if (Editor1.Text.Contains("\n"))
//                {
//                    Editor1.Text += "Инфа 100 \n";
//                }
//                else
//                {
//                    Editor1.Text = "Инфа 100 \n";
//                }
//            };
//<Button x:Name="Button1" Text="Узнай истину" BackgroundColor="Black" FontAttributes="Italic" HorizontalOptions="FillAndExpand" TextColor="Gray"/>
//        <Label x:Name="Editor1" VerticalOptions="Fill" HorizontalOptions="Fill" TextColor="Red" BackgroundColor="Black">
//            <Label.Text>Classified</Label.Text>
//        </Label>
//        <Button x:Name="GayButton" Text="А ты что считаешь?" Clicked="GayButton_Clicked" />
//        <Button x:Name="Where" Text="Ваш трансфер на***" Clicked="Where_Clicked"/>  