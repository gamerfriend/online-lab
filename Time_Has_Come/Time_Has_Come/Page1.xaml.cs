﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Time_Has_Come
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            InitializeComponent();
            Title = "Рассуждения о правильном применении оружия. Ди Грасси";
            Image0_0.Source = ImageSource.FromResource("Time_Has_Come.Photos.for_grassi.grassi1.jpg");            
            Image0_1.Source = ImageSource.FromResource("Time_Has_Come.Photos.for_grassi.gr_2_1.jpg");            
            Image1_0.Source = ImageSource.FromResource("Time_Has_Come.Photos.for_grassi.gr_3_4.jpg");            
            black_back0_0.Source = ImageSource.FromResource("Time_Has_Come.Photos.dark.png");
            black_back0_0.Clicked += (s, e) => { Page2 page1 = new Page2(); Navigation.PushAsync(page1); };
            black_back0_1.Source = ImageSource.FromResource("Time_Has_Come.Photos.dark.png");
            black_back0_1.Clicked += (s, e) => { Page3 page1 = new Page3(); Navigation.PushAsync(page1); };
            black_back1_0.Source = ImageSource.FromResource("Time_Has_Come.Photos.dark.png");
            black_back1_0.Clicked += (s, e) => { Page4 page1 = new Page4(); Navigation.PushAsync(page1); };
        }
    }
}