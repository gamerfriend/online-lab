﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Time_Has_Come
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {
        public Page2()
        {
            InitializeComponent();
            Title = "Итальянский двуручный меч маэстро Джакомо ди Грасси";
            Image1.Source = ImageSource.FromResource("Time_Has_Come.Photos.for_grassi.grassi1.jpg");
            Image3.Source = ImageSource.FromResource("Time_Has_Come.Photos.for_grassi.grassi3.jpg");
            Image2.Source = ImageSource.FromResource("Time_Has_Come.Photos.for_grassi.grassi2.jpg");
        }
    }
}